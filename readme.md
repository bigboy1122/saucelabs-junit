# Saucelabs Test

## Description
This project is meant to run a simple Selenium test through Sauce Labs using the Sauce Connect Docker image to create
a tunnel.  In the _.gitlab-ci.yml_ the sauce-connect docker image should be started as a service with the following 
variables declared  
````yaml
  SAUCE_TUNNEL_ID: ${SAUCE_TUNNEL_ID} # Sauce Connect TUNNEL ID, needs to also be set in the capabilities of the test
  SAUCE_USERNAME: ${SAUCE_USERNAME} # Sauce Labs user id, needs to be set in the selenium test as well
  SAUCE_API_KEY: ${SAUCE_API_KEY} # Sauce Labs API Key, needs to be set in the selenium test as well
````
Also add the service
````yaml
  services:
    - tgalati1122/sauce-connect:latest
````

Please view the [gitlab-ci.yml](.gitlab-ci.yml) in this project for the example

 